import java.util.Random;

public class Generator {

    final String UPPER_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    final String LOWER_LETTERS = UPPER_LETTERS.toLowerCase();
    final String DIGITS = "123456789";

    private Random random = new Random();
    private String[] randomTypes;
    private String[] types = {"String", "Date", "Integer", "Float"};



    /**
     * Метод для генерации имен колонок и их типов
     */
    public String[] genColumns(int numbOfColumns, int maxWordLength, int minWordLength){

        String[] randomColumns = new String[numbOfColumns];
        randomTypes = new String[numbOfColumns];
        for (int i=0; i<numbOfColumns; i++){
            randomTypes[i] = types[random.nextInt(types.length)];
            int wordLength = minWordLength + random.nextInt(maxWordLength); // определяем длину слова

            if(i == numbOfColumns-1){ // на последней итерации пробел не нужен
                randomColumns[i] = this.headStringGenerator(wordLength) + " " + randomTypes[i];
                break;
            }
            randomColumns[i] = this.headStringGenerator(wordLength) + " " + randomTypes[i] + "; "; // генерируем слова разной длины и добавляем случайный тип

        }
        return randomColumns;
    }

    /**
     * Метод для генерации всех рядов
     */
    public String[] genRows(int numbOfRows, int numbOfColumns, int maxWordLength, int dateLength, int integerLength, int floatLength){

        String[] randomRows = new String[numbOfRows];
        // сформируем все ряды
        for(int i=0; i<numbOfRows; i++) {
            // сформируем один ряд
            randomRows[i] = "";
            for (int j = 0; j < numbOfColumns; j++) {

                if (randomTypes[j].equals("String")) {
                    if(j == numbOfColumns-1) {randomRows[i] += this.stringGenerator(maxWordLength) + ""; break;} // на последней итерации убираем пробел
                    randomRows[i] += this.stringGenerator(maxWordLength) + "; "; // с каждой этого типа данных из колонок мы прибавляем к строка ряда сгенерированное значение нужного типа
                    continue;
                }
                if (randomTypes[j].equals("Date")) {
                    if(j == numbOfColumns-1) {randomRows[i] += this.dateGenerator(dateLength)+ ""; break;}
                    randomRows[i] += this.dateGenerator(dateLength)+ "; ";
                    continue;
                }
                if (randomTypes[j].equals("Integer")) {
                    if(j == numbOfColumns-1) {randomRows[i] += this.integerGenerator(integerLength)+ ""; break;}
                    randomRows[i] += this.integerGenerator(integerLength) + "; ";
                    continue;
                }
                if (randomTypes[j].equals("Float")) {
                    if(j == numbOfColumns-1) {randomRows[i] += this.floatGenerator(floatLength) + ""; break;}
                    randomRows[i] += this.floatGenerator(floatLength) + "; ";
                    continue;
                }

            }

        }
        return randomRows;
    }


    public String headStringGenerator(int wordLength) {
        String alphabeticalSet = UPPER_LETTERS + LOWER_LETTERS;
        char[] charSeq; // из которой будут выбираться сисмволы
        char[] genCharSeq; // в которую будут генерироваться
        charSeq = alphabeticalSet.toCharArray();

        genCharSeq = new char[wordLength];
        for (int i = 0; i < wordLength; i++) {
            genCharSeq[i] = charSeq[random.nextInt(alphabeticalSet.length())];
        }

        return new String(genCharSeq);
    }

    public String stringGenerator(int wordLength){

        String alphanumericSet = UPPER_LETTERS + LOWER_LETTERS + DIGITS;
        char[] charSeq; // из которой будут выбираться сисмволы
        char[] genCharSeq; // в которую будут генерироваться
        charSeq = alphanumericSet.toCharArray();

        genCharSeq = new char[wordLength];
        for (int i = 0; i < wordLength; i++) {// генерируем символы в зависимости от указанной длины слова
            genCharSeq[i] = charSeq[random.nextInt(alphanumericSet.length())]; // генерируется очередное случайное число из диапазона нашего набора, теперь оно - индекс по которому из набора символов берется символ
        }
        return new String(genCharSeq);
    }

    public String dateGenerator(int dateLength) {
        char[] digitSeq = DIGITS.toCharArray();
        char[] digitDayMonthSeq = ("0" + DIGITS.substring(0,3)).toCharArray(); // возьмем первые 3 символа из массива чисел-символов для первого числа в дне и месяце

        char[] genCharSeq = new char[dateLength];


        // генерируем день
        genCharSeq[0] = digitDayMonthSeq[random.nextInt(digitDayMonthSeq.length)]; // нет смысла делать цикл на 2 итерации, потому что каждая итерация все-равно обрабатывается по-своему и нужны будут доп условия в цикле
        if(genCharSeq[0] == '3') {
            genCharSeq[1] = digitDayMonthSeq[random.nextInt(2)]; // если это 30е числа, то тут второй символ- '0' либо '1'
        } else{
            genCharSeq[1] = digitSeq[random.nextInt(digitSeq.length)];
        }

        // генерируем месяц
        genCharSeq[2] = '.'; genCharSeq[5] = '.';
        genCharSeq[3] = digitDayMonthSeq[random.nextInt(2)]; // первый символ месяца либо '0' либо '1'
        if(genCharSeq[3] == '1'){
            genCharSeq[4] = digitDayMonthSeq[random.nextInt(3)]; // если месяц начинается с 1, то второй символ либо '0' либо '1' либо '2'
        } else{
            genCharSeq[4] = digitSeq[random.nextInt(digitSeq.length)];
        }

        //генерируем год
        for (int i = 6; i < 10; i++){
            genCharSeq[i] = digitSeq[random.nextInt(DIGITS.length())]; // здесь можно разные цифры
        }

        return new String(genCharSeq);
    }

    public String integerGenerator(int integerLength){
        char[] digitSeq = DIGITS.toCharArray();
        char[] genCharSeq;

        genCharSeq = new char[integerLength];
        for (int i = 0; i < integerLength; i++){
            genCharSeq[i] = digitSeq[random.nextInt(DIGITS.length())];
        }

        return new String(genCharSeq);
    }

    public String floatGenerator(int floatLength){
        char[] digitSeq = DIGITS.toCharArray();
        char[] genCharSeq;

        genCharSeq = new char[floatLength];
        int commaPlace = 1+ random.nextInt(floatLength -2); // запятая разместиться под таким индексом
        for (int i = 0; i < floatLength; i++){
            if(i == commaPlace){
                genCharSeq[i] = ',';
                continue;
            }
            genCharSeq[i] = digitSeq[random.nextInt(DIGITS.length())];
        }

        return new String(genCharSeq);
    }






}
